package com.pe.mibanco.currency.conversion.service;

import com.pe.mibanco.currency.conversion.entity.TipoCambio;
import com.pe.mibanco.currency.conversion.repository.TipoCambioRepository;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TipoCambioServiceImpl implements TipoCambioService{

  @Autowired
  private TipoCambioRepository tipoCambioRepository;

  @Override
  public Flux<TipoCambio> findAll() {
    return tipoCambioRepository.findAll();
  }

  @Override
  public Flux<TipoCambio> findTipoCambioByOrigenDestino(String codigoOrigen, String codigoDestino) {
    return tipoCambioRepository.findTipoCambioByOrigenDestino(codigoOrigen, codigoDestino);
  }

  @Override
  public Mono<TipoCambio> guardarTipoCambio(TipoCambio tipoCambio) {
    return tipoCambioRepository.save(tipoCambio);
  }
}
