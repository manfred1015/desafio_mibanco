package com.pe.mibanco.currency.conversion.entity;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;


@Setter
@Getter
@Document("ranking")
public class Ranking {

  @MongoId
  private String id;

  @NotEmpty
  @Field("codigo_pais")
  private String codigoPais;

  @NotEmpty
  @Field("nombre_pais")
  private String nombrePais;

  @NotNull
  @Field(name = "ranking_peso")
  private Integer rankingPeso;

  public Ranking(String codigoPais) {
    this.codigoPais = codigoPais;
  }

  public Ranking() {
  }
}
