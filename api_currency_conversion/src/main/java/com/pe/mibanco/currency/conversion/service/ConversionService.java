package com.pe.mibanco.currency.conversion.service;

import com.pe.mibanco.currency.conversion.dto.ConversionRequestDto;
import com.pe.mibanco.currency.conversion.dto.ConversionResponseDto;
import reactor.core.publisher.Mono;

public interface ConversionService {
  Mono<ConversionResponseDto> executeConversion (ConversionRequestDto conversionRequestDto);
}
