package com.pe.mibanco.currency.conversion.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class ConversionRequestDto {
  private BigDecimal monto;
  private String monedaOrigen;
  private String monedaDestino;
}
