package com.pe.mibanco.currency.conversion.service;

import com.pe.mibanco.currency.conversion.entity.TipoCambio;
import java.util.List;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TipoCambioService {

  Flux<TipoCambio> findAll();
  Flux<TipoCambio> findTipoCambioByOrigenDestino(String codigoOrigen, String codigoDestino);
  Mono<TipoCambio> guardarTipoCambio (TipoCambio tipoCambio);

}
