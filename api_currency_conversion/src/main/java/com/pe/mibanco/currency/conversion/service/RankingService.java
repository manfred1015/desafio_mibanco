package com.pe.mibanco.currency.conversion.service;

import com.pe.mibanco.currency.conversion.entity.Ranking;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RankingService {

  Flux<Ranking> findAll();
  Flux<Ranking> findByCodigoPais(String codigoPais);
  Mono<Ranking> save (Ranking ranking);

}
