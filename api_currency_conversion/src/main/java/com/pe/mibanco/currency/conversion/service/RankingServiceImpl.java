package com.pe.mibanco.currency.conversion.service;

import com.pe.mibanco.currency.conversion.entity.Ranking;
import com.pe.mibanco.currency.conversion.repository.RankingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RankingServiceImpl implements RankingService{

  @Autowired
  private RankingRepository rankingRepository;

  @Override
  public Flux<Ranking> findAll() {
    return rankingRepository.findAll();
  }

  @Override
  public Flux<Ranking> findByCodigoPais(String codigoPais) {
    return rankingRepository.findByCodigoPais(codigoPais);
  }

  @Override
  public Mono<Ranking> save(Ranking ranking) {
    return rankingRepository.save(ranking);
  }
}
