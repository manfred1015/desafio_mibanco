package com.pe.mibanco.currency.conversion.repository;

import com.pe.mibanco.currency.conversion.entity.Ranking;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface RankingRepository extends ReactiveMongoRepository<Ranking, String> {

  @Query("{ 'codigoPais': '?0' }")
  Flux<Ranking> findByCodigoPais (String codigoPais);

}
