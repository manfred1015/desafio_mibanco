package com.pe.mibanco.currency.conversion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCurrencyConversionApplication {

  public static void main(String[] args) {
    SpringApplication.run(ApiCurrencyConversionApplication.class, args);
  }

}
