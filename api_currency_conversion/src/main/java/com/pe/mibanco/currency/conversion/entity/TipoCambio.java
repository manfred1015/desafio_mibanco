package com.pe.mibanco.currency.conversion.entity;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document("tipoCambio")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TipoCambio {

  @MongoId
  private String id;

  @NotEmpty
  @Field("codigo_pais_origen")
  private String codigoPaisOrigen;

  @NotEmpty
  @Field("codigo_pais_destino")
  private String codigoPaisDestino;

  @NotNull
  @Field(name = "tipo_cambio")
  private BigDecimal tipoCambio;

}
