package com.pe.mibanco.currency.conversion.controller;

import com.pe.mibanco.currency.conversion.dto.ConversionRequestDto;
import com.pe.mibanco.currency.conversion.entity.Ranking;
import com.pe.mibanco.currency.conversion.service.ConversionService;
import jakarta.validation.Valid;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/conversion")
public class ConvesionController {

  @Autowired
  private ConversionService conversionService;

  @PostMapping
  public Mono<ResponseEntity<Map<String, Object>>> convertirDivisa (@Valid @RequestBody Mono<ConversionRequestDto> request) {
    Map<String, Object> respuesta = new HashMap<>();
    return request.flatMap(r -> {
      return conversionService.executeConversion(r)
          .map(client -> {
            respuesta.put("conversion", r);
            respuesta.put("mensaje", "Conversion exitosa.");
            respuesta.put("timestamp", new Date());
            return ResponseEntity.ok(respuesta);
          });
    }).onErrorResume(throwable -> {
      return Mono.just(throwable).cast(WebExchangeBindException.class)
          .flatMap(e -> Mono.just(e.getFieldErrors()))
          .flatMapMany(Flux::fromIterable)
          .map(fieldError -> "El campo: " + fieldError.getField() + " " + fieldError.getDefaultMessage())
          .collectList()
          .flatMap(list -> {
            respuesta.put("errors", list);
            respuesta.put("timestamp", new Date());
            respuesta.put("status", HttpStatus.BAD_REQUEST.value());
            return Mono.just(ResponseEntity.badRequest().body(respuesta));
          });
    });
  }

}
