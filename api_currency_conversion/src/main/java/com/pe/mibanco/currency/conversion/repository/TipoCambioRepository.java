package com.pe.mibanco.currency.conversion.repository;

import com.pe.mibanco.currency.conversion.entity.Ranking;
import com.pe.mibanco.currency.conversion.entity.TipoCambio;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface TipoCambioRepository extends ReactiveMongoRepository<TipoCambio, String> {

  @Query("{ 'codigoPaisOrigen': '?0' , 'codigoPaisDestino':  '?1'}")
  Flux<TipoCambio> findTipoCambioByOrigenDestino (String codigoPaisOrigen, String destino);

}
