package com.pe.mibanco.currency.conversion.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class ConversionResponseDto {
  private BigDecimal monto;
  private BigDecimal montoTipo;
  private String monedaOrigen;
  private String monedaDestino;
  private BigDecimal tipoCambio;
}
