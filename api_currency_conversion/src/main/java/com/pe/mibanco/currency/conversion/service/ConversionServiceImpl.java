package com.pe.mibanco.currency.conversion.service;

import com.pe.mibanco.currency.conversion.dto.ConversionRequestDto;
import com.pe.mibanco.currency.conversion.dto.ConversionResponseDto;
import com.pe.mibanco.currency.conversion.entity.Ranking;
import com.pe.mibanco.currency.conversion.entity.TipoCambio;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ConversionServiceImpl implements ConversionService{

  @Autowired
  private RankingService rankingService;

  @Autowired
  private TipoCambioService tipoCambioService;

  @Override
  public Mono<ConversionResponseDto> executeConversion(ConversionRequestDto request) {

    ConversionResponseDto conversionResponseDto = new ConversionResponseDto();
    BigDecimal tipoCambio;
    BigDecimal conversion;
    Flux<TipoCambio> listFluxTipoCambio;
    List<TipoCambio> tipoCambioList = null;

    Flux<Ranking> listFluxRankingOrigen = this.rankingService.findByCodigoPais(request.getMonedaOrigen());
    Mono<List<Ranking>> rankingOrigenListMono = listFluxRankingOrigen.collectList();
    List<Ranking> lstRankingOrigen = rankingOrigenListMono.block();

    Flux<Ranking> listFluxRankingDestino = this.rankingService.findByCodigoPais(request.getMonedaDestino());
    Mono<List<Ranking>> rankingDestinoListMono = listFluxRankingDestino.collectList();
    List<Ranking> lstRankingsDestino = rankingDestinoListMono.block();


    Integer rankingOrigen = lstRankingOrigen.get(0).getRankingPeso();
    Integer rankingDestino = lstRankingsDestino.get(0).getRankingPeso();

    if (rankingOrigen > rankingDestino) {

      listFluxTipoCambio = this.tipoCambioService.findTipoCambioByOrigenDestino(
          request.getMonedaOrigen(), request.getMonedaDestino());

      tipoCambioList = listFluxTipoCambio.collectSortedList().block();

      tipoCambio = tipoCambioList.get(0).getTipoCambio();

      conversion = request.getMonto().multiply(tipoCambio);

      conversionResponseDto.setMontoTipo(conversion);
      conversionResponseDto.setMonto(request.getMonto());
      conversionResponseDto.setTipoCambio(tipoCambio);
      conversionResponseDto.setMonedaOrigen(request.getMonedaOrigen());
      conversionResponseDto.setMonedaDestino(request.getMonedaDestino());
    } else {
      tipoCambio = tipoCambioList.get(0).getTipoCambio();
      conversion = request.getMonto().divide(tipoCambio);
      conversionResponseDto.setMontoTipo(conversion);
      conversionResponseDto.setMonto(request.getMonto());
      conversionResponseDto.setTipoCambio(tipoCambio);
      conversionResponseDto.setMonedaOrigen(request.getMonedaOrigen());
      conversionResponseDto.setMonedaDestino(request.getMonedaDestino());
    }

    return Mono.just(conversionResponseDto);
  }
}
