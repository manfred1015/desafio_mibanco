package com.pe.mibanco.currency.conversion.controller;

import com.pe.mibanco.currency.conversion.entity.Ranking;
import com.pe.mibanco.currency.conversion.service.RankingService;
import jakarta.validation.Valid;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/ranking")
public class RankingController {

  @Autowired
  private RankingService rankingService;

  @GetMapping
  public Mono<ResponseEntity<Flux<Ranking>>> listarRanking () {
    return Mono.just(
        ResponseEntity.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(rankingService.findAll())
    );
  }

  @GetMapping("/{codigoPais}")
  public Mono<ResponseEntity<Flux<Ranking>>> getRankingByCodigoPais (@PathVariable String codigoPais) {
    return Mono.just(
        ResponseEntity.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(rankingService.findByCodigoPais(codigoPais))
    ).defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @PostMapping
  public Mono<ResponseEntity<Map<String, Object>>> guardarCliente (@Valid @RequestBody Mono<Ranking> ranking) {
    Map<String, Object> respuesta = new HashMap<>();
    return ranking.flatMap(r -> {
      return rankingService.save(r)
          .map(client -> {
            respuesta.put("ranking", r);
            respuesta.put("mensaje", "Registro exitoso de ranking.");
            respuesta.put("timestamp", new Date());
            return ResponseEntity
                .created(URI.create("/api/v1/ranking".concat(r.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(respuesta);
          });
    }).onErrorResume(throwable -> {
      return Mono.just(throwable).cast(WebExchangeBindException.class)
          .flatMap(e -> Mono.just(e.getFieldErrors()))
          .flatMapMany(Flux::fromIterable)
          .map(fieldError -> "El campo: " + fieldError.getField() + " " + fieldError.getDefaultMessage())
          .collectList()
          .flatMap(list -> {
            respuesta.put("errors", list);
            respuesta.put("timestamp", new Date());
            respuesta.put("status", HttpStatus.BAD_REQUEST.value());
            return Mono.just(ResponseEntity.badRequest().body(respuesta));
          });
    });
  }



}
